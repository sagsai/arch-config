# overrides /usr/lib/python3.10/site-packages/qutebrowser/config/configdata.yml
# A Lot of the mappings are lost due to overriding and not reassigning
# @TODO Add them here
config.load_autoconfig(True)

config.unbind("f", mode="normal")
config.unbind("F", mode="normal")
config.unbind("j", mode="normal")
config.unbind("k", mode="normal")
config.unbind("h", mode="normal")
config.unbind("l", mode="normal")
config.unbind("i", mode="normal")
config.unbind("gg", mode="normal")
config.unbind("gi", mode="normal")
config.unbind("G", mode="normal")
config.unbind("n", mode="normal")
config.unbind("N", mode="normal")
config.unbind("o", mode="normal")
config.unbind("O", mode="normal")
config.unbind("xo", mode="normal")
config.unbind("xO", mode="normal")
config.unbind("wo", mode="normal")
config.unbind("wO", mode="normal")
config.unbind("d", mode="normal")
config.unbind("D", mode="normal")
config.unbind("ga", mode="normal")
config.unbind("gC", mode="normal")
config.unbind("J", mode="normal")
config.unbind("K", mode="normal")
config.unbind("co", mode="normal")
config.unbind("gD", mode="normal")
config.unbind("T", mode="normal")
config.unbind("<Ctrl+^>", mode="normal")
config.unbind("g0", mode="normal")
config.unbind("g^", mode="normal")
config.unbind("g$", mode="normal")
config.unbind("gm", mode="normal")
config.unbind("gK", mode="normal")
config.unbind("gJ", mode="normal")
config.unbind("gt", mode="normal")
config.unbind("H", mode="normal")
config.unbind("L", mode="normal")
config.unbind("go", mode="normal")
config.unbind("gO", mode="normal")
config.unbind("gu", mode="normal")
config.unbind("gU", mode="normal")
config.unbind("th", mode="normal")
config.unbind("wf", mode="normal")
config.unbind("wh", mode="normal")
config.unbind("tl", mode="normal")
config.unbind("wl", mode="normal")
config.unbind("wi", mode="normal")
config.unbind("wIh", mode="normal")
config.unbind("wIj", mode="normal")
config.unbind("wIk", mode="normal")
config.unbind("wIl", mode="normal")
config.unbind("wIw", mode="normal")
config.unbind("wIf", mode="normal")
config.unbind("gd", mode="normal")
config.unbind("ad", mode="normal")
config.unbind("cd", mode="normal")
config.unbind("tsh", mode="normal")
config.unbind("tSh", mode="normal")
config.unbind("tsH", mode="normal")
config.unbind("tSH", mode="normal")
config.unbind("tsu", mode="normal")
config.unbind("tSu", mode="normal")
config.unbind("tph", mode="normal")
config.unbind("tPh", mode="normal")
config.unbind("tpH", mode="normal")
config.unbind("tPH", mode="normal")
config.unbind("tpu", mode="normal")
config.unbind("tPu", mode="normal")
config.unbind("tih", mode="normal")
config.unbind("tIh", mode="normal")
config.unbind("tiH", mode="normal")
config.unbind("tIH", mode="normal")
config.unbind("tiu", mode="normal")
config.unbind("tIu", mode="normal")
config.unbind("tch", mode="normal")
config.unbind("tCh", mode="normal")
config.unbind("tcH", mode="normal")
config.unbind("tCH", mode="normal")
config.unbind("tcu", mode="normal")
config.unbind("tCu", mode="normal")
config.unbind("<Ctrl+f>", mode="normal")
config.unbind("<Ctrl+b>", mode="normal")
config.unbind("<Alt+1>", mode="normal")
config.unbind("<Alt+2>", mode="normal")
config.unbind("<Alt+3>", mode="normal")
config.unbind("<Alt+4>", mode="normal")
config.unbind("<Alt+5>", mode="normal")
config.unbind("<Alt+6>", mode="normal")
config.unbind("<Alt+7>", mode="normal")
config.unbind("<Alt+8>", mode="normal")
config.unbind("<Alt+9>", mode="normal")
config.unbind("<Alt+m>", mode="normal")
config.unbind("<Alt+m>", mode="normal")
config.unbind("<F5>", mode="normal")
config.unbind("<Ctrl+F5>", mode="normal")
config.unbind("<Ctrl+a>", mode="normal")
config.unbind("<Ctrl+x>", mode="normal")
config.unbind("<Ctrl+PgDown>", mode="normal")
config.unbind("<Ctrl+PgUp>", mode="normal")
config.unbind("<Ctrl+Shift+t>", mode="normal")
config.unbind("<Ctrl+w>", mode="normal")
config.unbind("<Forward>", mode="normal")
config.unbind("<Back>", mode="normal")
config.unbind("<F11>", mode="normal")

config.bind("ff", "hint", mode="normal")
config.bind("fi", "hint inputs --first", mode="normal")
config.bind("FF", "hint all tab", mode="normal")
config.bind("FW", "hint all window", mode="normal")
# config.bind(";m", "spawn mpv {url}", mode="normal")
config.bind(";m", "hint links spawn mpv {hint-url}", mode="normal")
config.bind(";M", "hint --rapid links spawn mpv {hint-url}", mode="normal")

config.bind("n", "scroll up", mode="normal")
config.bind("e", "scroll down", mode="normal")
config.bind("i", "scroll left", mode="normal")
config.bind("o", "scroll right", mode="normal")

config.bind("<Alt-n>", "back", mode="normal")
config.bind("<Alt-e>", "forward", mode="normal")
config.bind("<Alt-i>", "forward -t", mode="normal")
config.bind("<Alt-o>", "back -t", mode="normal")
config.bind("<Ctrl-i>", "back -w", mode="normal")
config.bind("<Ctrl-o>", "forward -w", mode="normal")

config.bind("N", "scroll-page 0 -1", mode="normal")
config.bind("E", "scroll-page 0 1", mode="normal")
config.bind("ge", "scroll-to-perc", mode="normal")
config.bind("gn", "scroll-to-perc 0", mode="normal")

config.bind("h", "search-next", mode="normal")
config.bind("H", "search-prev", mode="normal")

config.bind("^", "navigate up", mode="normal")
config.bind("<Ctrl+^>", "navigate up -t", mode="normal")
config.bind(">>", "navigate increment", mode="normal")
config.bind("<<", "navigate decrement", mode="normal")

config.bind("gi", "mode-enter insert", mode="normal")

# o set-cmd-text -s :open
# go set-cmd-text :open {url:pretty}
# Change the url of the current tab
config.bind("l", "set-cmd-text :open {url:pretty}", mode="normal")
# O set-cmd-text -s :open -t
# gO set-cmd-text :open -t -r {url:pretty}
# Open the specified url in a new tab and switch to it
config.bind("L", "set-cmd-text :open -t -r {url:pretty}", mode="normal")
# xo set-cmd-text -s :open -b
# xO set-cmd-text :open -b -r {url:pretty}
# Open the specified url in a new tab but don't switch to it
config.bind("<Ctrl-l>", "set-cmd-text :open -b -r {url:pretty}", mode="normal")
# wo set-cmd-text -s :open -w
# wO set-cmd-text :open -w {url:pretty}
# Open the specified url in a new window
config.bind("<Alt-l>", "set-cmd-text :open -w {url:pretty}", mode="normal")
# config.bind("ta", "open -t ;; set-cmd-text :open https://", mode="normal")
config.bind("wa", "open -t ;; set-cmd-text :open ", mode="normal")
config.bind("wq", "tab-close", mode="normal")
config.bind("wQ", "tab-close -o", mode="normal")

config.bind("wc", "tab-clone", mode="normal")

config.bind("wn", "tab-prev", mode="normal")
config.bind("we", "tab-next", mode="normal")

config.bind("wp", "tab-pin", mode="normal")
config.bind("wu", "tab-mute", mode="normal")
config.bind("wy", "tab-only", mode="normal")
config.bind("wd", "tab-give", mode="normal")

config.bind("wJ", "set-cmd-text -s :tab-select", mode="normal")
config.bind("wj", "set-cmd-text -sr :tab-focus", mode="normal")
config.bind("w1", "tab-focus 1", mode="normal")
config.bind("w2", "tab-focus 2", mode="normal")
config.bind("w3", "tab-focus 3", mode="normal")
config.bind("w4", "tab-focus 4", mode="normal")
config.bind("w5", "tab-focus 5", mode="normal")
config.bind("w6", "tab-focus 6", mode="normal")
config.bind("w7", "tab-focus 7", mode="normal")
config.bind("w8", "tab-focus 8", mode="normal")
config.bind("w9", "tab-focus 9", mode="normal")
config.bind("w0", "tab-focus 10", mode="normal")
config.bind("w$", "tab-focus -1", mode="normal")

config.bind("wm", "tab-move", mode="normal")
config.bind("wN", "tab-move -", mode="normal")
config.bind("wE", "tab-move +", mode="normal")

config.bind("dt", "devtools", mode="normal")
config.bind("df", "devtools-focus", mode="normal")
config.bind("di", "devtools left", mode="normal")
config.bind("do", "devtools right", mode="normal")
config.bind("dn", "devtools top", mode="normal")
config.bind("de", "devtools bottom", mode="normal")

config.bind("DD", "download", mode="normal")
config.bind("DC", "download-cancel", mode="normal")
config.bind("DL", "download-clear", mode="normal")

config.bind(
    "cj",
    "config-cycle -p -t -u *://{url:host}/* content.javascript.enabled ;; reload",
    mode="normal",
)
config.bind(
    "cJ",
    "config-cycle -p -u *://{url:host}/* content.javascript.enabled ;; reload",
    mode="normal",
)
config.bind(
    "cv",
    "config-cycle -p -t -u *://*.{url:host}/* content.javascript.enabled ;; reload",
    mode="normal",
)
config.bind(
    "cV",
    "config-cycle -p -u *://*.{url:host}/* content.javascript.enabled ;; reload",
    mode="normal",
)
config.bind(
    "cjv",
    "config-cycle -p -t -u {url} content.javascript.enabled ;; reload",
    mode="normal",
)
config.bind(
    "cJV",
    "config-cycle -p -u {url} content.javascript.enabled ;; reload",
    mode="normal",
)
config.bind(
    "cp",
    "config-cycle -p -t -u *://{url:host}/* content.plugins ;; reload",
    mode="normal",
)
config.bind(
    "cP",
    "config-cycle -p -u *://{url:host}/* content.plugins ;; reload",
    mode="normal",
)
config.bind(
    "cl",
    "config-cycle -p -t -u *://*.{url:host}/* content.plugins ;; reload",
    mode="normal",
)
config.bind(
    "cL",
    "config-cycle -p -u *://*.{url:host}/* content.plugins ;; reload",
    mode="normal",
)
config.bind(
    "cpl",
    "config-cycle -p -t -u {url} content.plugins ;; reload",
    mode="normal",
)
config.bind(
    "cPL",
    "config-cycle -p -u {url} content.plugins ;; reload",
    mode="normal",
)
config.bind(
    "ci",
    "config-cycle -p -t -u *://{url:host}/* content.images ;; reload",
    mode="normal",
)
config.bind(
    "cI",
    "config-cycle -p -u *://{url:host}/* content.images ;; reload",
    mode="normal",
)
config.bind(
    "cm",
    "config-cycle -p -t -u *://*.{url:host}/* content.images ;; reload",
    mode="normal",
)
config.bind(
    "cM",
    "config-cycle -p -u *://*.{url:host}/* content.images ;; reload",
    mode="normal",
)
config.bind(
    "cii",
    "config-cycle -p -t -u {url} content.images ;; reload",
    mode="normal",
)
config.bind(
    "cmm",
    "config-cycle -p -u {url} content.images ;; reload",
    mode="normal",
)
config.bind(
    "cc",
    "config-cycle -p -t -u *://{url:host}/* content.cookies.accept all no-3rdparty never ;; reload",
    mode="normal",
)
config.bind(
    "cC",
    "config-cycle -p -u *://{url:host}/* content.cookies.accept all no-3rdparty never ;; reload",
    mode="normal",
)
config.bind(
    "ck",
    "config-cycle -p -t -u *://*.{url:host}/* content.cookies.accept all no-3rdparty never ;; reload",
    mode="normal",
)
config.bind(
    "cK",
    "config-cycle -p -u *://*.{url:host}/* content.cookies.accept all no-3rdparty never ;; reload",
    mode="normal",
)
config.bind(
    "ccc",
    "config-cycle -p -t -u {url} content.cookies.accept all no-3rdparty never ;; reload",
    mode="normal",
)
config.bind(
    "ckk",
    "config-cycle -p -u {url} content.cookies.accept all no-3rdparty never ;; reload",
    mode="normal",
)

# <Ctrl+n> open -w
# <Ctrl+Shift+n> open -p

# Caret Mode
# H scroll left
# J scroll down
# K scroll up
# L scroll right

config.unbind("h", mode="caret")
config.unbind("j", mode="caret")
config.unbind("k", mode="caret")
config.unbind("l", mode="caret")
config.unbind("H", mode="caret")
config.unbind("J", mode="caret")
config.unbind("K", mode="caret")
config.unbind("L", mode="caret")
config.unbind("e", mode="caret")
config.unbind("w", mode="caret")
config.unbind("b", mode="caret")
config.unbind("o", mode="caret")
config.unbind("gg", mode="caret")
config.unbind("G", mode="caret")
config.bind("n", "move-to-prev-line", mode="caret")
config.bind("e", "move-to-next-line", mode="caret")
config.bind("o", "move-to-prev-char", mode="caret")
config.bind("i", "move-to-next-char", mode="caret")
config.bind("N", "scroll up", mode="caret")
config.bind("E", "scroll down", mode="caret")
config.bind("I", "scroll left", mode="caret")
config.bind("O", "scroll right", mode="caret")

config.bind("t", "move-to-end-of-word", mode="caret")
config.bind("s", "move-to-next-word", mode="caret")
config.bind("a", "move-to-prev-word", mode="caret")
# config.bind("t", "move-to-end-of-word", mode="caret")
config.bind("gn", "move-to-start-of-document", mode="caret")
config.bind("ge", "move-to-end-of-document", mode="caret")
config.bind("<Ctrl-o>", "selection-reverse", mode="caret")

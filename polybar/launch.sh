#!/bin/sh

# Terminate already running bar instances
killall -q polybar

# Launch Polybar, using default config location ~/.config/polybar/config.ini
polybar bar1 2>&1 | tee -a /tmp/polybar.log & disown
polybar bar2 2>&1 | tee -a /tmp/polybar.log & disown

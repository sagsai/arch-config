-- Read the docs: https://www.lunarvim.org/docs/configuration
-- Video Tutorials: https://www.youtube.com/watch?v=sFA9kX-Ud_c&list=PLhoH5vyxr6QqGu0i7tt_XoVK9v-KvZ3m6
-- Forum: https://www.reddit.com/r/lunarvim/
-- Discord: https://discord.com/invite/Xb9B4Ny

-- vim options
vim.opt.relativenumber = true
vim.opt.expandtab = false -- dont convert tabs to spaces
vim.opt.shiftwidth = 4    -- the number of spaces inserted for each indentation
vim.opt.tabstop = 4       -- insert 4 spaces for a tab
vim.opt.listchars:append("tab:>=,space:.,eol:↴")
vim.opt.scrolloff = 0
vim.opt.spell = true
vim.opt.spelllang = "en"

-- general
lvim.log.level = "info"
lvim.format_on_save = true
--[[ lvim.format_on_save = {
	enabled = true,
	pattern = "*.lua",
	timeout = 1000,
} ]]
-- to disable icons and use a minimalist setup, uncomment the following
-- lvim.use_icons = false

-- lvim.icons.ui.Folder = "󰉋"
-- lvim.builtin.nvimtree.setup.renderer.icons.glyphs.folder.default = lvim.icons.ui.Folder

vim.cmd([[
	" Remember cursor position between vim sessions
	autocmd BufReadPost *
		\ if line("'\"") > 0 && line ("'\"") <= line("$") |
		\   exe "normal! g'\"" |
		\ endif

	" set the grepping tool for project search
	set diffopt+=indent-heuristic
	set diffopt+=algorithm:histogram
	" ignore whitespaces
	set diffopt+=iwhite

	set grepprg=rg\ --vimgrep\ --no-heading\ --smart-case
	set grepformat=%f:%l:%c:%m,%f:%l:%m

	let g:markdown_fenced_languages = ['html', 'css', 'javascript', 'php', 'json', 'xml', 'bash']
]])

-- keymappings <https://www.lunarvim.org/docs/configuration/keybindings>
lvim.leader = "space"
-- add your own keymapping
lvim.keys.normal_mode["<C-s>"] = ":w<cr>"
vim.keymap.set("i", "<C-s>", "<ESC>:w<CR>i")
vim.keymap.set("v", "<C-s>", ":w<CR>")

-- lvim.keys.normal_mode["<S-l>"] = ":BufferLineCycleNext<CR>"
-- lvim.keys.normal_mode["<S-h>"] = ":BufferLineCyclePrev<CR>"

-- -- Use which-key to add extra bindings with the leader-key prefix
-- lvim.builtin.which_key.mappings["W"] = { "<cmd>noautocmd w<cr>", "Save without formatting" }
-- lvim.builtin.which_key.mappings["P"] = { "<cmd>Telescope projects<CR>", "Projects" }

if lvim.keys.normal_mode.j ~= nil and lvim.keys.normal_mode.j.k ~= nil then
	-- Replace Alt-j and Alt-k keymappings with <leader>n and <leader>o respectively
	-- to move line up and down
	vim.keymap.del("n", "<M-j>")
	vim.keymap.del("n", "<M-k>")
	vim.keymap.del("i", "<M-j>")
	vim.keymap.del("i", "<M-k>")
end

-- a -> motion: previous word beginning
-- A -> motion: previous word(with special characters) beginning
-- b -> motion: before/until a search character right to the cursor
-- B -> motion: after a search character left to the cursor
-- c -> change (as before)
-- C -> case operations, Commenting
-- d -> delete (as before)
-- D -> diagnostic/lsp/codelens
-- e -> go 1 line down
-- E -> page down
-- f -> find character after cursor (as before) [non-repetitive, CAN BE MADE kf]
-- F -> find character before cursor (as before) [non-repetitive, CAN BE MADE KF/kF]
-- g -> goto+
-- G -> git related stuff
-- h -> next search term occurance
-- H -> previous search term occurance
-- i -> go 1 character left
-- I -> position cursor at the top of the screen
-- j -> jump/hop, motion around (previously a) e.g. motion j" when cursor in "some text" will include surrounding "
-- J -> @TODO
-- k -> Command+
-- K -> @TODO Command+
-- l -> Line operations
-- L -> More line operations
-- m -> motion/text object inner(previously i), mark manipulation
-- M -> Method jumping
-- n -> go 1 line above
-- N -> page up
-- o -> go 1 character right
-- O -> position cursor at the bottom of the screen
-- p -> paste after the cursor (as before) [repetition done with ., CAN BE MADE kp]
-- P -> paste before the cursor (as before) [repetition done with ., CAN BE MADE kP]
-- q -> record (as before) [non repetitive, CAN BE MADE kq]
-- Q -> @TODO
-- r -> motion: previous word end
-- s -> motion: next word beginning
-- t -> motion: next word end
-- u -> undo (as before)
-- U -> redo
-- v -> visual (as before) CAN BE MADE kv
-- V -> Linewise visual (as before) CAN BE MADE kV
-- w -> wrap
-- W -> tab related
-- x -> delete character under cursor
-- X -> delete character(s) left to the cursor
-- y -> yank (as before) [non-repetitive, CAN BE MADE ky]
-- Y -> @TODO (originally same as y$)
-- z -> fold
-- Z -> @TODO
-- ' -> goto mark
-- " => register
lvim.lsp.buffer_mappings.normal_mode['K'] = nil  -- vim.lsp.buf.hover() Show hover
lvim.lsp.buffer_mappings.normal_mode['gd'] = nil -- vim.lsp.buf.definition() Goto definition
lvim.lsp.buffer_mappings.normal_mode['gD'] = nil -- vim.lsp.buf.declaration() Goto Declaration
lvim.lsp.buffer_mappings.normal_mode['gg'] = nil -- vim.lsp.buf.implementation() Goto Implementation
lvim.lsp.buffer_mappings.normal_mode['gI'] = nil -- vim.lsp.buf.implementation() Goto Implementation
lvim.lsp.buffer_mappings.normal_mode['gr'] = nil -- vim.lsp.buf.references() Goto references
lvim.lsp.buffer_mappings.normal_mode['gs'] = nil -- vim.lsp.buf.signature_help() Show signature help
-- vim.keymap.set("o", "K", "<NOP>")  -- vim.lsp.buf.hover() Show hover
-- vim.keymap.set("n", "gd", "<NOP>") -- vim.lsp.buf.definition() Goto definition
-- vim.keymap.set("n", "gD", "<NOP>") -- vim.lsp.buf.declaration() Goto Declaration
-- vim.keymap.set("n", "gg", "<NOP>") -- vim.lsp.buf.implementation() Goto Implementation
-- vim.keymap.set("n", "gI", "<NOP>") -- vim.lsp.buf.implementation() Goto Implementation
-- vim.keymap.set("n", "gr", "<NOP>") -- vim.lsp.buf.references() Goto references
-- vim.keymap.set("n", "gs", "<NOP>") -- vim.lsp.buf.signature_help() Show signature help

-- @NOTE a->b mapping breaking nvim-surround
vim.keymap.set("o", "a", "b", { desc = "Previous word beginning" })
vim.keymap.set("o", "r", "ge", { desc = "Previous word end" })
vim.keymap.set("o", "s", "w", { desc = "Next word beginning" })
vim.keymap.set("o", "t", "e", { desc = "Next word end" })
vim.keymap.set("o", "A", "B", { desc = "Previous word beginning including special characters" })
vim.keymap.set("o", "R", "gE", { desc = "Previous word end including special characters" })
vim.keymap.set("o", "S", "W", { desc = "Nexword beginning including special characters" })
vim.keymap.set("o", "T", "E", { desc = "Next word end including special characters" })

-- vim.keymap.del("n", "dm")
vim.keymap.set("o", "n", "k", { desc = "Up one line" })
vim.keymap.set("o", "e", "j", { desc = "Down one line" })
vim.keymap.set("o", "i", "h", { desc = "Left one character" })
vim.keymap.set("o", "o", "l", { desc = "Right one character" })
vim.keymap.set("o", "I", "H", { desc = "Place cursor at the top line of the screen" })
vim.keymap.set("o", "O", "L", { desc = "Place cursor at the bottom line of the screen" })

vim.keymap.set("o", "m", "i", { desc = "In e.g. dm) deletes the content inside ()" })
vim.keymap.set("o", "h", "n", { desc = "Find next" })
vim.keymap.set("o", "H", "N", { desc = "Find previous" })
vim.keymap.set("o", "b", "t", { desc = "Place cursor before the next occurance of the character" })
vim.keymap.set("o", "B", "T", { desc = "Place cursor after the previous occurance of the character" })
-- vim.keymap.set("o", "l", "_", { desc="current line(then downward if any count attached before)" })
vim.keymap.set("o", "j", "a", { desc = "Around e.g. dj) deletes the content and the surrounding ()" })

vim.keymap.set("o", "gn", "gg", { desc = "Beginning of the file" })
vim.keymap.set("o", "ge", "G", { desc = "End of the file" })
vim.keymap.set("o", "MA", "[m", { desc = "Prev method start" })
vim.keymap.set("o", "MR", "[M", { desc = "Prev method end" })
vim.keymap.set("o", "MS", "]m", { desc = "Next method start" })
vim.keymap.set("o", "MT", "]M", { desc = "Next method end" })

vim.keymap.set("o", "cgE", "<NOP>")
vim.keymap.set("o", "E", "<NOP>")
vim.keymap.set("o", "k", "<NOP>")
vim.keymap.set("o", "l", "<NOP>")
vim.keymap.set("o", "L", "<NOP>")
vim.keymap.set("o", "N", "<NOP>")
vim.keymap.set("o", "w", "iw")
vim.keymap.set("o", "W", "iW")

vim.keymap.set("n", "C", "<NOP>")
vim.keymap.del("n", "C")
vim.keymap.set("n", "D", "<NOP>")
vim.keymap.del("n", "D")
vim.keymap.set("n", "gE", "<NOP>")
vim.keymap.set("n", "gl", "<NOP>") -- show line error
vim.keymap.set("n", "G", "<NOP>")
vim.keymap.set("n", "k", "<NOP>")
vim.keymap.set("n", "kk", "<NOP>")
vim.keymap.set("n", "K", "<NOP>") -- <cmd>lua vim.lsp.buf.hover()<cr>
vim.keymap.set("n", "l", "<NOP>")
vim.keymap.set("n", "L", "<NOP>")
vim.keymap.set("n", "m", "<NOP>")
vim.keymap.set("n", "w", "<NOP>")
vim.keymap.set("n", "W", "<NOP>")
vim.keymap.set("n", "Y", "<NOP>")
-- vim.keymap.set("n", "ge", "<NOP>")
-- vim.keymap.set("n", "dm", "<NOP>")

-- deletion should not interfere with copy-paste operation
vim.keymap.set("n", "c", '"_c')
vim.keymap.set("n", "cc", '"_cc')
vim.keymap.set("n", "x", '"_x')
vim.keymap.set("n", "X", '"_X')

-- override the default keymappings
vim.keymap.set("n", "a", "b", { desc = "Prev word beginning" })
vim.keymap.set("n", "r", "ge", { desc = "Prev word end" })
vim.keymap.set("n", "s", "w", { desc = "Next word beginning" })
vim.keymap.set("n", "t", "e", { desc = "Next word end" })
vim.keymap.set("n", "A", "B", { desc = "Prev word beginning including special characters" })
vim.keymap.set("n", "R", "gE", { desc = "Prev word end including special characters" })
vim.keymap.set("n", "S", "W", { desc = "Next word beginning including special characters" })
vim.keymap.set("n", "T", "E", { desc = "Next word end including special characters" })

vim.keymap.set("n", "n", "k", { desc = "Up" })
vim.keymap.set("n", "e", "j", { desc = "Down" })
vim.keymap.set("n", "i", "h", { desc = "Left" })
vim.keymap.set("n", "o", "l", { desc = "Right" })
vim.keymap.set("n", "N", "<PAGEUP>", { desc = "Page up" })
vim.keymap.set("n", "E", "<PAGEDOWN>", { desc = "Page down" })
vim.keymap.set("n", "I", "H", { desc = "Screen top" })
vim.keymap.set("n", "O", "L", { desc = "Screen bottom" })
vim.keymap.set("n", "<C-n>", ":m .-2<CR>==", { desc = "Move line up" })
vim.keymap.set("n", "<C-e>", ":m .+1<CR>==", { desc = "Move line down" })

-- free up t/T
vim.keymap.set("n", "b", "t", { desc = "Goto (b)efore" })
vim.keymap.set("n", "B", "T", { desc = "Goto (B)efore but from the opposite end" })

vim.keymap.set("n", "clt", '"_c$', { desc = "Change till the enD of line" })
vim.keymap.set("n", "cla", 'l"_c^', { desc = "Change till the Begging of the line" })
vim.keymap.set("n", "c%", 'gg0"_cG', { desc = "Change the entire content of the current buffer" })
vim.keymap.set("n", "<C-c>", "<C-o>", { desc = "Goto last cursor position" })

-- mnemonic: initials of CASE LOWER/UPPER/SWITCH WORD/LINE
vim.keymap.set("n", "CLW", "guiw", { desc = "Make the CURRENT word lowercase" })
vim.keymap.set("n", "CUW", "gUiw", { desc = "Make the current word uppercase" })
vim.keymap.set("n", "CSW", "g~iw", { desc = "Switch the case of the current word" })
vim.keymap.set("n", "CLL", "guu", { desc = "Make the current line lowercase" })
vim.keymap.set("n", "CUL", "gUU", { desc = "Make the current line uppercase" })
vim.keymap.set("n", "CSL", "g~~", { desc = "Switch the case of the current line" })

vim.keymap.set("n", "dlt", "D", { desc = "Delete till the enD of line" })
vim.keymap.set("n", "dla", "ld^", { desc = "Delete till the Beginning of the line" })
-- vim.keymap.set("n", "ddL", '"_d^', { desc="delete till the Beginning of the line" })
vim.keymap.set("n", "d%", ":%d", { desc = "Delete the entire content of the buffer" })
-- D -> diagnostic stuff after the plugins

vim.keymap.set("n", "gn", "gg0", { desc = "Begging of the buffer" })
vim.keymap.set("n", "ge", "G$", { desc = "End of the buffer" })
-- @TODO add/edit/delete other keymaps starting with g
-- G -> git related stuff after the plugins

vim.keymap.set("n", "h", "nzz", { desc = "Next match in the middle of the screen" })
vim.keymap.set("n", "H", "Nzz", { desc = "Prev match in the middle of the screen" })

-- @TODO free up j, & use something else for the following
vim.keymap.set("n", "jn", "[`", { desc = "Jump to previous lowercase mark" })
vim.keymap.set("n", "je", "]`", { desc = "Jump to next lowercase mark" })
vim.keymap.set("n", "ji", "['", { desc = "Jump to previous line with a lowercase mark" })
vim.keymap.set("n", "jo", "]'", { desc = "Jump to next line with a lowercase mark" })
-- @TODO free up J and assign something else to it

vim.keymap.set("n", "ki", "i", { desc = "Kommand: insert" })
vim.keymap.set("n", "kI", "I", { desc = "Kommand: insert at the beginning of the current line" })
-- free up r
vim.keymap.set("n", "kr", "r", { desc = "Kommand: Replace a single character" })
-- free up a/a
vim.keymap.set("n", "ka", "a", { desc = "kommand: append" })
vim.keymap.set("n", "kA", "A", { desc = "Kommand: APPEND at the end of the current line" })
vim.keymap.set("n", "ks", "s", { desc = "Kommand: substitute" })
vim.keymap.set("n", "kc", "i<C-r>=", { desc = "Kommand: calculate" })

-- @TODO K

-- Composite keymaps that work on lines
vim.keymap.set("n", "la", "k0", { desc = "Goto the beginning of the previous line" })
vim.keymap.set("n", "lr", "k$", { desc = "Goto the end of the previous line" })
vim.keymap.set("n", "ls", "j0", { desc = "Goto the beginning of the next line" })
vim.keymap.set("n", "lt", "j$", { desc = "Goto the end of the next line" })
vim.keymap.set("n", "le", "o", { desc = "Open Line after and edit" })
vim.keymap.set("n", "ln", "O", { desc = "Open Line before and edit" })
-- free up s
vim.keymap.set("n", "<M-l>", "o<ESC>k", { desc = "Add an empty line below" })
vim.keymap.set("n", "<M-L>", "O<ESC>j", { desc = "Add an empty line above" })

-- L -> Line operations
vim.keymap.set("n", "LE", "o<ESC>", { desc = "Open Line after and exit insert mode" })
vim.keymap.set("n", "LN", "O<ESC>", { desc = "Open Line before and exit insert mode" })
vim.keymap.set("n", "LJ", "gJ", { desc = "Join lines without space in between" })
vim.keymap.set("n", "Lj", "J", { desc = "Join lines with space in between" })
vim.keymap.set("n", "LI", ">>", { desc = "Indent the current line" })
vim.keymap.set("n", "LO", "<<", { desc = "Outdent the current line" })
vim.keymap.set("x", "LJ", "gJ", { desc = "Join lines without space in between" })
vim.keymap.set("x", "Lj", "J", { desc = "Join lines with space in between" })
vim.keymap.set("n", "LR", "R", { desc = "Replace till the end of the current line" })
vim.keymap.set("n", "LS", "S", { desc = "Substitute line" })

vim.keymap.set("n", "MA", "[m", { desc = "Goto prev method start" })
vim.keymap.set("n", "MR", "[M", { desc = "Goto prev method end" })
vim.keymap.set("n", "MS", "]m", { desc = "Goto next method start" })
vim.keymap.set("n", "MT", "]M", { desc = "Goto next method end" })

-- @TODO maybe remap P to Alt-p & use P for something else
-- @TODO maybe free up q & Q and use them for something else

vim.keymap.set("n", "<C-y>p", ':let @+ = expand("%:p")<CR>', { desc = "copy current buffer's path" })

-- @TODO maybe use atl-u and free up U
vim.keymap.set("n", "U", "<C-r>", { desc = "Redo" })

-- @TODO maybe assign v & V to kv & kV and use them for something else

-- think work, window, or wuffer
-- vim.keymap.set("n", "ws", ':w<CR>')
-- vim.keymap.set("n", "ww", ':wq<CR>')
-- vim.keymap.set("n", "WW", ':q<CR>')
-- w -> also for tabs(BufferLine) after the plugins

-- @TODO maybe assign X to alt-X & use X for some other purpose

vim.keymap.set("n", "yl", "y$", { desc = "Copy till the enD of line" })
vim.keymap.set("n", "yL", "ly^", { desc = "Copy till the Beginning of the line" })
vim.keymap.set("n", "y%", ":%y+<CR>", { desc = "Copy the entire content of the current buffer" })

-- @TODO Y

vim.keymap.set("n", "zn", "zk", { desc = "End of previous fold" })
vim.keymap.set("n", "ze", "zj", { desc = "Start of next fold" })
vim.keymap.set("n", "zno", "zn", { desc = "No folding" })

-- @TODO Z

-- deletion should not interfere with copy-paste operation
vim.keymap.set("v", "x", '"_x')
vim.keymap.set("v", "X", '"_X')
vim.keymap.set("v", "c", '"_c')

-- move the cursor to the opposite end of the selection
vim.keymap.set("v", "<M-o>", "o")
vim.keymap.set("v", "<M-O>", "O")

-- vim.keymap.set("x", "ge", "<NOP>")
vim.keymap.set("x", "gE", "<NOP>")
vim.keymap.set("x", "l", "<NOP>")
vim.keymap.set("x", "K", "<NOP>")
vim.keymap.set("x", "L", "<NOP>")
vim.keymap.set("x", "w", "<NOP>")
vim.keymap.set("x", "W", "<NOP>")
vim.keymap.set("x", "J", "<NOP>")
lvim.keys.normal_mode["J"] = nil
lvim.keys.visual_mode["J"] = false

-- make sure that the printable characters are not affected in the select mode
vim.keymap.set("x", "a", "b", { desc = "Select till prev word beginning" })
vim.keymap.set("x", "r", "ge", { desc = "Select till next word end" })
vim.keymap.set("x", "s", "w", { desc = "Select till next word beginning" })
vim.keymap.set("x", "t", "e", { desc = "Select till prev word end" })
vim.keymap.set("x", "A", "B", { desc = "Select till prev word beginning including special characters" })
vim.keymap.set("x", "R", "gE", { desc = "Select till prev word end including special characters" })
vim.keymap.set("x", "S", "W", { desc = "Select till next word beginning including special characters" })
vim.keymap.set("x", "T", "E", { desc = "Select till next word end including special characters" })

vim.keymap.set("x", "n", "k", { desc = "Select till above line" })
vim.keymap.set("x", "N", "<PAGEUP>", { desc = "Select till page up" })
vim.keymap.set("x", "e", "j", { desc = "Select till below line" })
vim.keymap.set("x", "E", "<PAGEDOWN>", { desc = "Select till page down" })
vim.keymap.set("x", "i", "h", { desc = "Select till left character" })
vim.keymap.set("x", "I", "H", { desc = "Select till screen top" })
vim.keymap.set("x", "o", "l", { desc = "Select till right character" })
vim.keymap.set("x", "O", "L", { desc = "Select till screen bottom" })

vim.keymap.set("x", "<C-n>", ":move '<-2<CR>gv-gv", { desc = "Move selection up" })
vim.keymap.set("x", "<C-e>", ":move '>+1<CR>gv-gv", { desc = "Move selection down" })

vim.keymap.set("x", "b", "t", { desc = "Select untill (b)efore" })
vim.keymap.set("x", "B", "T", { desc = "Select untill (B)efore but from the opposite end" })
vim.keymap.set("x", "gn", "gg", { desc = "Select till begging of the buffer" })
vim.keymap.set("x", "ge", "G", { desc = "Select till end of the buffer" })
vim.keymap.set("x", "h", "n", { desc = "Select till next search result" })
vim.keymap.set("x", "H", "N", { desc = "Select till prev search result" })
vim.keymap.set("x", "j", "a", { desc = "Select around ) } ] etc." })
vim.keymap.set("x", "JL", "gJ", { desc = "Join selected lines without space in between" })
vim.keymap.set("x", "Jl", "J", { desc = "Join selected lines with space in between" })
vim.keymap.set("x", "p", "p", { desc = "Replace the selection by the pasted stuff" })

vim.keymap.set("x", "ki", "i", { desc = "Kommand: insert" })
vim.keymap.set("x", "kI", "I", { desc = "Kommand: insert at the beginning of the block selection" })
-- free up r
vim.keymap.set("x", "kr", "r", { desc = "Kommand: Replace a single character" })
-- free up a/a
vim.keymap.set("x", "ka", "a", { desc = "Kommand: append" })
vim.keymap.set("x", "kA", "A", { desc = "Kommand: APPEND at the end of the block selection" })

vim.keymap.set("x", "MA", "[m", { desc = "Select till prev method start" })
vim.keymap.set("x", "MR", "[M", { desc = "Select till prev method end" })
vim.keymap.set("x", "MS", "]m", { desc = "Select till next method start" })
vim.keymap.set("x", "MT", "]M", { desc = "Select till next method end" })

lvim.builtin.autopairs.fast_wrap.map = "<M-p>"
vim.keymap.set("i", "<M-n>", "<Up>")
vim.keymap.set("i", "<M-e>", "<Down>")
vim.keymap.set("i", "<M-i>", "<Left>")
vim.keymap.set("i", "<M-o>", "<Right>")

vim.keymap.set("i", "<M-a>", "<C-o>b")
vim.keymap.set("i", "<M-r>", "<C-o>ge")
vim.keymap.set("i", "<M-s>", "<C-o>w")
vim.keymap.set("i", "<M-t>", "<C-o>e")
vim.keymap.set("i", "<M-A>", "<C-o>B")
vim.keymap.set("i", "<M-R>", "<C-o>gE")
vim.keymap.set("i", "<M-S>", "<C-o>W")
vim.keymap.set("i", "<M-T>", "<C-o>E")

vim.keymap.set("i", "<M-u>", "<C-o>u", { desc = "Undo" })
vim.keymap.set("i", "<M-U>", "<C-o><C-r>", { desc = "Redo" })
vim.keymap.set("i", "<M-0>", "<C-o>0", { desc = "Goto begging of the line" })
vim.keymap.set("i", "<M-4>", "<C-o>$", { desc = "Goto the end of the line" })
vim.keymap.set("i", "<C-v>", '<Esc>"+pa', { desc = "Paste from system clipboard" })
vim.keymap.set("i", "<M-N>", "<ESC>:m .-2<CR>==a", { desc = "Move line up" })
vim.keymap.set("i", "<M-E>", "<ESC>:m .+1<CR>==a", { desc = "Move line down" })

-- -- Change theme settings
-- lvim.colorscheme = "lunar"
lvim.colorscheme = "onedarker"

-- After changing plugin config exit and reopen LunarVim, Run :PackerSync
lvim.builtin.alpha.active = true
lvim.builtin.alpha.mode = "dashboard"
lvim.builtin.terminal.active = true
lvim.builtin.nvimtree.setup.view.side = "left"
lvim.builtin.nvimtree.setup.renderer.icons.show.git = false
-- hide the git ignored and dot files, can be toggled by I and H
lvim.builtin.nvimtree.setup.git.ignore = true
lvim.builtin.nvimtree.setup.filters.dotfiles = true
-- change key mappings

local function my_on_attach(bufnr)
	local api = require('nvim-tree.api')

	local function opts(desc)
		return { desc = "nvim-tree: " .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
	end

	-- Load the default kemappings from /~/.local/share/lunarvim/site/pack/lazy/opt/nvim-tree.lua/lua/nvim-tree/keymap.lua
	api.config.mappings.default_on_attach(bufnr)

	vim.keymap.del("n", "d", { buffer = bufnr })
	vim.keymap.del("n", "D", { buffer = bufnr })
	vim.keymap.del("n", "e", { buffer = bufnr })
	vim.keymap.del("n", "E", { buffer = bufnr })
	vim.keymap.del("n", "<C-e>", { buffer = bufnr })
	vim.keymap.del("n", "<C-r>", { buffer = bufnr })
	vim.keymap.del("n", "I", { buffer = bufnr })
	-- vim.keymap.del("n", "J", { buffer = bufnr })
	vim.keymap.del("n", "K", { buffer = bufnr })
	vim.keymap.del("n", "o", { buffer = bufnr })
	vim.keymap.del("n", "O", { buffer = bufnr })
	vim.keymap.del("n", "P", { buffer = bufnr })
	vim.keymap.del("n", "r", { buffer = bufnr })
	vim.keymap.del("n", "x", { buffer = bufnr })
	vim.keymap.del("n", "y", { buffer = bufnr })
	vim.keymap.del("n", "Y", { buffer = bufnr })
	vim.keymap.del("n", "gy", { buffer = bufnr })

	vim.keymap.set('n', 'rr', api.fs.rename_sub, opts('Rename: Omit Filename'))
	vim.keymap.set('n', 'rb', api.fs.rename_basename, opts('Rename: Basename'))
	vim.keymap.set('n', 'rn', api.fs.rename, opts('Rename'))

	vim.keymap.set('n', 'i', api.node.navigate.sibling.prev, opts('Previous Sibling'))
	vim.keymap.set('n', 'o', api.node.navigate.sibling.next, opts('Next Sibling'))
	vim.keymap.set('n', 'I', api.node.navigate.sibling.first, opts('First Sibling'))
	vim.keymap.set('n', 'O', api.node.navigate.sibling.last, opts('Last Sibling'))

	vim.keymap.set('n', 'GI', api.tree.toggle_gitignore_filter, opts('Toggle Git Ignore'))
	vim.keymap.set('n', 'GC', api.tree.toggle_git_clean_filter, opts('Toggle Git Clean'))
	vim.keymap.set('n', 'gn', api.node.navigate.git.prev, opts('Prev Git'))
	vim.keymap.set('n', 'ge', api.node.navigate.git.next, opts('Next Git'))

	vim.keymap.set('n', 'yy', api.fs.copy.node, opts('Copy'))
	vim.keymap.set('n', 'yp', api.fs.copy.absolute_path, opts('Copy Absolute Path'))
	vim.keymap.set('n', 'yn', api.fs.copy.filename, opts('Copy Name'))
	vim.keymap.set('n', 'yP', api.fs.copy.relative_path, opts('Copy Relative Path'))

	vim.keymap.set('n', '<BS>', api.node.navigate.parent, opts('Parent Directory'))

	vim.keymap.set('n', '<CR>', api.node.open.edit, opts('Open'))
	vim.keymap.set('n', '<2-LeftMouse>', api.node.open.edit, opts('Open'))

	vim.keymap.set('n', 'dd', api.fs.cut, opts('Cut'))
	vim.keymap.set('n', 'D', api.fs.remove, opts('Delete'))
	vim.keymap.set('n', 'dt', api.fs.trash, opts('Trash'))

	-- vim.keymap.set('n', '<C-]>', api.tree.change_root_to_node,          opts('CD'))
	--   vim.keymap.set('n', '<C-e>', api.node.open.replace_tree_buffer,     opts('Open: In Place'))
	--   vim.keymap.set('n', '<C-k>', api.node.show_info_popup,              opts('Info'))
	--   vim.keymap.set('n', '<C-t>', api.node.open.tab,                     opts('Open: New Tab'))
	--   vim.keymap.set('n', '<C-v>', api.node.open.vertical,                opts('Open: Vertical Split'))
	--   vim.keymap.set('n', '<C-x>', api.node.open.horizontal,              opts('Open: Horizontal Split'))
	--   vim.keymap.set('n', '<BS>',  api.node.navigate.parent_close,        opts('Close Directory'))
	--   vim.keymap.set('n', '<Tab>', api.node.open.preview,                 opts('Open Preview'))
	--   vim.keymap.set('n', '.',     api.node.run.cmd,                      opts('Run Command'))
	--   vim.keymap.set('n', '-',     api.tree.change_root_to_parent,        opts('Up'))
	--   vim.keymap.set('n', 'a',     api.fs.create,                         opts('Create'))
	--   vim.keymap.set('n', 'bmv',   api.marks.bulk.move,                   opts('Move Bookmarked'))
	--   vim.keymap.set('n', 'B',     api.tree.toggle_no_buffer_filter,      opts('Toggle No Buffer'))
	--   vim.keymap.set('n', 'E',     api.tree.expand_all,                   opts('Expand All'))
	--   vim.keymap.set('n', ']e',    api.node.navigate.diagnostics.next,    opts('Next Diagnostic'))
	--   vim.keymap.set('n', '[e',    api.node.navigate.diagnostics.prev,    opts('Prev Diagnostic'))
	--   vim.keymap.set('n', 'F',     api.live_filter.clear,                 opts('Clean Filter'))
	--   vim.keymap.set('n', 'f',     api.live_filter.start,                 opts('Filter'))
	--   vim.keymap.set('n', 'g?',    api.tree.toggle_help,                  opts('Help'))
	--   vim.keymap.set('n', 'H',     api.tree.toggle_hidden_filter,         opts('Toggle Dotfiles'))
	--   vim.keymap.set('n', 'm',     api.marks.toggle,                      opts('Toggle Bookmark'))
	--   vim.keymap.set('n', 'O',     api.node.open.no_window_picker,        opts('Open: No Window Picker'))
	-- vim.keymap.set('n', 'p',     api.fs.paste,                          opts('Paste'))
	-- vim.keymap.set('n', 'q',     api.tree.close,                        opts('Close'))
	-- vim.keymap.set('n', 'R',     api.tree.reload,                       opts('Refresh'))
	-- vim.keymap.set('n', 's',     api.node.run.system,                   opts('Run System'))
	-- vim.keymap.set('n', 'S',     api.tree.search_node,                  opts('Search'))
	-- vim.keymap.set('n', 'U',     api.tree.toggle_custom_filter,         opts('Toggle Hidden'))
	-- vim.keymap.set('n', 'W',     api.tree.collapse_all,                 opts('Collapse'))
	-- vim.keymap.set('n', '<2-RightMouse>', api.tree.change_root_to_node, opts('CD'))
end

lvim.builtin.nvimtree.setup.on_attach = my_on_attach

-- TODO remove the filename from the top breadcrumbs
lvim.builtin.breadcrumbs.active = true

-- Automatically install missing parsers when entering buffer
lvim.builtin.treesitter.auto_install = true

-- lvim.builtin.treesitter.ignore_install = { "haskell" }

-- -- generic LSP settings <https://www.lunarvim.org/docs/languages#lsp-support>

-- --- disable automatic installation of servers
-- lvim.lsp.installer.setup.automatic_installation = false

-- ---configure a server manually. IMPORTANT: Requires `:LvimCacheReset` to take effect
-- ---see the full default list `:lua =lvim.lsp.automatic_configuration.skipped_servers`
-- vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "pyright" })
-- local opts = {} -- check the lspconfig documentation for a list of all possible options
-- require("lvim.lsp.manager").setup("pyright", opts)

-- ---remove a server from the skipped list, e.g. eslint, or emmet_ls. IMPORTANT: Requires `:LvimCacheReset` to take effect
-- ---`:LvimInfo` lists which server(s) are skipped for the current filetype
-- lvim.lsp.automatic_configuration.skipped_servers = vim.tbl_filter(function(server)
--   return server ~= "emmet_ls"
-- end, lvim.lsp.automatic_configuration.skipped_servers)

-- -- you can set a custom on_attach function that will be used for all the language servers
-- -- See <https://github.com/neovim/nvim-lspconfig#keybindings-and-completion>
-- lvim.lsp.on_attach_callback = function(client, bufnr)
--   local function buf_set_option(...)
--     vim.api.nvim_buf_set_option(bufnr, ...)
--   end
--   --Enable completion triggered by <c-x><c-o>
--   buf_set_option("omnifunc", "v:lua.vim.lsp.omnifunc")
-- end

-- -- linters and formatters <https://www.lunarvim.org/docs/languages#lintingformatting>
local formatters = require "lvim.lsp.null-ls.formatters"
formatters.setup {
	{ command = "eslint_d",      filetypes = { "javascript" } },
	{ command = "jq",            filetypes = { "json" } },
	{ command = "tidy",          filetypes = { "html" } },
	{ command = "stylelint",     filetypes = { "css" } },
	{ command = "sql-formatter", filetypes = { "sql" } },
	-- { command = "yapf", filetypes = { "python" } },
	{ command = "black",         filetypes = { "python" } },
	--   { command = "stylua" },
	--   {
	--     command = "prettier",
	--     extra_args = { "--print-width", "100" },
	--     filetypes = { "typescript", "typescriptreact" },
	--   },
}

local linters = require "lvim.lsp.null-ls.linters"
linters.setup {
	{ command = "eslint_d",   filetypes = { "javascript" } },
	{ command = "jsonlint",   filetypes = { "json" } },
	{ command = "php",        filetypes = { "php" } },
	{ command = "tidy",       filetypes = { "html" } },
	{ command = "stylelint",  filetypes = { "css" } },
	{ command = "sqlfluff",   filetypes = { "sql" } },
	{ command = "shellcheck", filetypes = { "sh" },        args = { "--severity", "warning" } },
	-- { command = "mypy", filetypes = { "python" } },
	{ command = "pylint",     filetypes = { "python" } },
	-- { command = "flake8", filetypes = { "python" } },
	--   {
	--     command = "shellcheck",
	--     args = { "--severity", "warning" },
	--   },
}

-- -- Additional Plugins <https://www.lunarvim.org/docs/plugins#user-plugins>
lvim.plugins = {
	{
		"folke/todo-comments.nvim",
		dependencies = {
			"nvim-lua/plenary.nvim",
		},
		-- event = "BufRead",
		config = function()
			require("todo-comments").setup({
				signs = true, -- show icons in the signs column
				sign_priority = 8, -- sign priority
				-- keywords recognized as todo comments
				keywords = {
					FIXME = {
						icon = " ", -- icon used for the sign, and in search results
						color = "error", -- can be a hex color, or a named color (see below)
						alt = { "FIX", "BUG", "FIXIT", "ISSUE" }, -- a set of other keywords that all map to this FIX keywords
						-- signs = false, -- configure signs for some keywords individually
					},
					TODO = { icon = " ", color = "info" },
					HACK = { icon = " ", color = "warning" },
					WARN = { icon = " ", color = "warning", alt = { "WARNING", "XXX" } },
					PERF = { icon = " ", alt = { "OPTIM", "PERFORMANCE", "OPTIMIZE" } },
					NOTE = { icon = " ", color = "hint", alt = { "INFO" } },
					TEST = { icon = "⏲ ", color = "test", alt = { "TESTING", "PASSED", "FAILED" } },
				},
				gui_style = {
					fg = "NONE", -- The gui style to use for the fg highlight group.
					bg = "BOLD", -- The gui style to use for the bg highlight group.
				},
				merge_keywords = true, -- when true, custom keywords will be merged with the defaults
				-- highlighting of the line containing the todo comment
				-- * before: highlights before the keyword (typically comment characters)
				-- * keyword: highlights of the keyword
				-- * after: highlights after the keyword (todo text)
				highlight = {
					multiline = true, -- enable multine todo comments
					multiline_pattern = "^.", -- lua pattern to match the next multiline from the start of the matched keyword
					multiline_context = 10, -- extra lines that will be re-evaluated when changing a line
					before = "",     -- "fg" or "bg" or empty
					keyword = "wide", -- "fg", "bg", "wide" or empty. (wide is the same as bg, but will also highlight surrounding characters)
					after = "fg",    -- "fg" or "bg" or empty
					pattern = [[.*<(KEYWORDS)\s*]], -- pattern or table of patterns, used for highlightng (vim regex)
					comments_only = true, -- uses treesitter to match keywords in comments only
					max_line_len = 400, -- ignore lines longer than this
					exclude = {},    -- list of file types to exclude highlighting
				},
				-- list of named colors where we try to extract the guifg from the
				-- list of hilight groups or use the hex color if hl not found as a fallback
				colors = {
					error = { "DiagnosticError", "ErrorMsg", "#DC2626" },
					warning = { "DiagnosticWarning", "WarningMsg", "#FBBF24" },
					info = { "DiagnosticInfo", "#2563EB" },
					hint = { "DiagnosticHint", "#10B981" },
					default = { "Identifier", "#7C3AED" },
					test = { "Identifier", "#FF00FF" }
				},
				search = {
					command = "rg",
					args = {
						"--color=never",
						"--no-heading",
						"--with-filename",
						"--line-number",
						"--column",
					},
					-- regex that will be used to match keywords.
					-- don't replace the (KEYWORDS) placeholder
					-- pattern = [[\b(KEYWORDS):]], -- ripgrep regex
					pattern = [[\b(KEYWORDS)\b]], -- match without the extra colon. You'll likely get false positives
				},
			})
		end,
	},

	-- Code annotation/doc generator
	{
		"kkoomen/vim-doge",
		build = ":call doge#install()",
		config = function()
			vim.g.doge_enable_mappings = 0
			-- vim.g.doge_doc_standard_lua = "ldoc"
			-- vim.g.doge_doc_standard_python = "google"
			-- vim.g.doge_doc_standard_javascript = "jsdoc"
			-- vim.g.doge_doc_standard_typescript = "jsdoc"
			-- vim.g.doge_doc_standard_rs = "rustdoc"
			vim.g.doge_mapping_comment_jump_forward = "<Tab>"
			vim.g.doge_mapping_comment_jump_backward = "<S-Tab>"
			vim.g.doge_buffer_mappings = 1
			vim.g.doge_comment_jump_modes = { "n", "i", "s" }
			-- vim.g.doge_mapping = "<Leader>?"
		end,
		cmd = { "DogeGenerate", "DogeCreateDocStandard" },
		enabled = true,
	},

	{
		"folke/trouble.nvim",
		cmd = "TroubleToggle",
	},

	-- FIXME after remapping a key and plugin update, no longer works
	{
		"kylechui/nvim-surround",
		version = "*",
		-- event = "VeryLazy",
		config = function()
			require("nvim-surround").setup({
				-- Configuration here, or leave empty to use defaults
				keymaps = {
					normal = "ww", -- surround followed by text object/movement
					normal_line = "wW", -- surround followed by text object/movement, with a newline & indentation
					normal_cur = "wl", -- surround the entire line
					normal_cur_line = "wL", -- surround the entire line, with a newline & indentation
					delete = "wd", -- Delete/remove surround
					change = "wc", -- modify surround
					change_line = "wC", -- modify the surround beyond newline & indentation
					visual = "ww",
					visual_line = "wW"
					-- insert = "<C-g>s",
					-- insert_line = "<C-g>S"
				},
				surrounds = {
					["("] = {
						add = { "( ", " )" },
						find = function()
							local config = require("nvim-surround.config")
							return config.get_selection({ motion = "j(" })
						end,
						delete = "^(. ?)().-( ?.)()$",
					},
					[")"] = {
						add = { "(", ")" },
						find = function()
							local config = require("nvim-surround.config")
							return config.get_selection({ motion = "j)" })
						end,
						delete = "^(.)().-(.)()$",
					},
					["{"] = {
						add = { "{ ", " }" },
						find = function()
							local config = require("nvim-surround.config")
							return config.get_selection({ motion = "j{" })
						end,
						delete = "^(. ?)().-( ?.)()$",
					},
					["}"] = {
						add = { "{", "}" },
						find = function()
							local config = require("nvim-surround.config")
							return config.get_selection({ motion = "j}" })
						end,
						delete = "^(.)().-(.)()$",
					},
					["<"] = {
						add = { "< ", " >" },
						find = function()
							local config = require("nvim-surround.config")
							return config.get_selection({ motion = "j<" })
						end,
						delete = "^(. ?)().-( ?.)()$",
					},
					[">"] = {
						add = { "<", ">" },
						find = function()
							local config = require("nvim-surround.config")
							return config.get_selection({ motion = "j>" })
						end,
						delete = "^(.)().-(.)()$",
					},
					["["] = {
						add = { "[ ", " ]" },
						find = function()
							local config = require("nvim-surround.config")
							return config.get_selection({ motion = "j[" })
						end,
						delete = "^(. ?)().-( ?.)()$",
					},
					["]"] = {
						add = { "[", "]" },
						find = function()
							local config = require("nvim-surround.config")
							return config.get_selection({ motion = "j]" })
						end,
						delete = "^(.)().-(.)()$",
					},
					["'"] = {
						add = { "'", "'" },
						find = function()
							local config = require("nvim-surround.config")
							return config.get_selection({ motion = "j'" })
						end,
						delete = "^(.)().-(.)()$",
					},
					['"'] = {
						add = { '"', '"' },
						find = function()
							local config = require("nvim-surround.config")
							return config.get_selection({ motion = 'j"' })
						end,
						delete = "^(.)().-(.)()$",
					},
					["`"] = {
						add = { "`", "`" },
						find = function()
							local config = require("nvim-surround.config")
							return config.get_selection({ motion = "j`" })
						end,
						delete = "^(.)().-(.)()$",
					},
				}
			})
		end
	},

	{
		"phaazon/hop.nvim",
		event = "BufRead",
		config = function()
			require("hop").setup()
			vim.api.nvim_set_keymap("n", "jj", ":HopChar2<CR>", { silent = true })
			vim.api.nvim_set_keymap("n", "jw", ":HopWord<CR>", { silent = true })
			vim.api.nvim_set_keymap("n", "jl", ":HopLine<CR>", { silent = true })
			vim.api.nvim_set_keymap("n", "ja", ":HopAnywhere<CR>", { silent = true })
		end,
	},

	{
		"windwp/nvim-ts-autotag",
		config = function()
			require("nvim-ts-autotag").setup()
		end,
	},

	-- Latest Lunarvim shows and updates the breadcrumbs as a file is scrolled, so the following plugin is not needed
	-- {
	-- 	"romgrk/nvim-treesitter-context",
	-- 	config = function()
	-- 		require("treesitter-context").setup {
	-- 			enable = true, -- Enable this plugin (Can be enabled/disabled later via commands)
	-- 			throttle = true, -- Throttles plugin updates (may improve performance)
	-- 			max_lines = 0, -- How many lines the window should span. Values <= 0 mean no limit.
	-- 			patterns = { -- Match patterns for TS nodes. These get wrapped to match at word boundaries.
	-- 				-- For all filetypes
	-- 				-- Note that setting an entry here replaces all other patterns for this entry.
	-- 				-- By setting the 'default' entry below, you can control which nodes you want to
	-- 				-- appear in the context window.
	-- 				default = {
	-- 					'class',
	-- 					'function',
	-- 					'method',
	-- 				},
	-- 			},
	-- 		}
	-- 	end
	-- },

	{
		"norcalli/nvim-colorizer.lua",
		config = function()
			require("colorizer").setup({ "css", "scss", "html", "javascript" }, {
				RGB = true, -- #RGB hex codes
				RRGGBB = true, -- #RRGGBB hex codes
				RRGGBBAA = true, -- #RRGGBBAA hex codes
				rgb_fn = true, -- CSS rgb() and rgba() functions
				hsl_fn = true, -- CSS hsl() and hsla() functions
				css = true, -- Enable all CSS features: rgb_fn, hsl_fn, names, RGB, RRGGBB
				css_fn = true, -- Enable all CSS *functions*: rgb_fn, hsl_fn
			})
		end,
	},

	{
		"ray-x/lsp_signature.nvim",
		event = "BufRead",
		config = function() require "lsp_signature".on_attach() end,
	},

	{
		-- @FIXME setting focus_location from o to m not working
		"simrat39/symbols-outline.nvim",
		cmd = "SymbolsOutline",
		config = function()
			require("symbols-outline").setup {
				keymaps = {
					focus_location = "m",
				},
			}
			-- vim.g.symbols_outline = {
			-- 	keymaps = {
			-- 		focus_location = "f",
			-- 	},
			-- }
		end,
	},

	-- {
	-- 	"lukas-reineke/indent-blankline.nvim",
	-- 	event = "BufRead",
	-- 	setup = function()
	-- 		vim.g.indentLine_enabled = 1
	-- 		vim.g.indent_blankline_char = "▏"
	-- 		vim.g.indent_blankline_filetype_exclude = { "help", "terminal", "dashboard" }
	-- 		vim.g.indent_blankline_buftype_exclude = { "terminal" }
	-- 		vim.g.indent_blankline_show_trailing_blankline_indent = false
	-- 		vim.g.indent_blankline_show_first_indent_level = false
	-- 	end
	-- },

	{
		"folke/persistence.nvim",
		event = "BufReadPre", -- this will only start session saving when an actual file was opened
		lazy = true,
		config = function()
			require("persistence").setup {
				dir = vim.fn.expand(vim.fn.stdpath("config") .. "/sessions/"),
				need = 1,
				branch = true, -- use git branch to save session
				options = { "buffers", "curdir", "tabpages", "winsize" },
			}
		end,
	},

	{
		"folke/lsp-colors.nvim",
		event = "BufRead",
	},

	{
		'gelguy/wilder.nvim',
		config = function()
			local wilder = require('wilder')
			wilder.setup({ modes = { ':', '/', '?' } })

			wilder.set_option('pipeline', {
				wilder.branch(
					wilder.python_file_finder_pipeline({
						file_command = { 'rg', '--files' },
						dir_command = { 'fd', '-td' },
						filters = { 'fuzzy_filter', 'difflib_sorter' },
					}),
					wilder.substitute_pipeline({
						pipeline = wilder.python_search_pipeline({
							skip_cmdtype_check = 1,
							pattern = wilder.python_fuzzy_pattern({
								start_at_boundary = 0,
							}),
						}),
					}),
					wilder.cmdline_pipeline({
						-- sets the language to use, 'vim' and 'python' are supported
						language = 'python',
						-- 0 turns off fuzzy matching
						-- 1 turns on fuzzy matching
						-- 2 partial fuzzy matching (match does not have to begin with the same first letter)
						fuzzy = 1,
						-- fuzzy = 2,
					}),
					{
						wilder.check(function(ctx, x) return x == '' end),
						wilder.history(),
					},
					wilder.python_search_pipeline({
						-- can be set to wilder#python_fuzzy_delimiter_pattern() for stricter fuzzy matching
						pattern = wilder.python_fuzzy_pattern(),
						-- omit to get results in the order they appear in the buffer
						sorter = wilder.python_difflib_sorter(),
						-- can be set to 're2' for performance, requires pyre2 to be installed
						-- see :h wilder#python_search() for more details
						engine = 're2',
					})
				),
			})

			wilder.set_option('renderer', wilder.popupmenu_renderer(
				wilder.popupmenu_border_theme({
					highlighter = wilder.basic_highlighter(),
					min_width = '100%', -- minimum height of the popupmenu, can also be a number
					min_height = '50%', -- to set a fixed height, set max_height to the same value
					reverse = 0, -- if 1, shows the candidates from bottom to top
				})
			))
		end
	},

	{
		'vimwiki/vimwiki',
		config = function()
			vim.cmd([[
				let g:vimwiki_list = [{'path': '~/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}]
				nmap l <Plug>VimwikiListo
				nmap L <Plug>VimwikiListO
			]])
		end
	},

	-- {
	-- 	"iamcco/markdown-preview.nvim",
	-- 	cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
	-- 	build = "cd app && npm install",
	-- 	init = function()
	-- 		vim.g.mkdp_auto_start = 1
	-- 		vim.g.mkdp_filetypes = { "markdown" }
	-- 	end,
	-- 	-- init = function() vim.g.mkdp_filetypes = { "markdown" } end,
	-- 	-- config = function()
	-- 	-- vim.cmd([[
	-- 	-- 	" let g:mkdp_browser = '/usr/bin/google-chrome-stable %U'
	-- 	-- 	let g:mkdp_browser = '/usr/bin/google-chrome-stable'
	-- 	-- 	nmap <C-p> <Plug>MarkdownPreviewToggle
	-- 	-- ]])
	-- 	-- end
	-- 	ft = { "markdown" },
	-- },

	{
		'jwalton512/vim-blade'
	},

	{
		'mattn/emmet-vim',
		config = function()
			vim.cmd([[
				let g:user_emmet_mode='i'
				" let g:user_emmet_leader_key='<C-m>'
				let g:user_emmet_install_global = 0
				autocmd FileType html,css EmmetInstall
			]])
		end
	},

	-- preview the search result as a line in the quickfix window is selected
	{
		"kevinhwang91/nvim-bqf",
		event = { "BufRead", "BufNew" },
		config = function()
			require("bqf").setup({
				auto_enable = true,
				preview = {
					win_height = 12,
					win_vheight = 12,
					delay_syntax = 80,
					border_chars = { "┃", "┃", "━", "━", "┏", "┓", "┗", "┛", "█" },
				},
				func_map = {
					vsplit = "",
					ptogglemode = "z,",
					stoggleup = "",
				},
				filter = {
					fzf = {
						action_for = { ["ctrl-s"] = "split" },
						extra_opts = { "--bind", "ctrl-o:toggle-all", "--prompt", "> " },
					},
				},
			})
		end,
	},

	-- search and replace
	{
		"windwp/nvim-spectre",
		event = "BufRead",
		config = function()
			require("spectre").setup()
		end,
	},

	-- preview the line which is being typed after :
	{
		"nacro90/numb.nvim",
		event = "BufRead",
		config = function()
			require("numb").setup {
				show_numbers = true, -- Enable 'number' for the window while peeking
				show_cursorline = true, -- Enable 'cursorline' for the window while peeking
			}
		end,
	},

	-- {
	-- 	"ms-jpq/chadtree",
	-- 	run = "python3 -m chadtree deps",
	-- },

	{
		"kevinhwang91/rnvimr",
		cmd = "RnvimrToggle",
		config = function()
			vim.g.rnvimr_enable_ex = 1
			vim.g.rnvimr_draw_border = 1
			vim.g.rnvimr_enable_picker = 1
			vim.g.rnvimr_enable_bw = 1
			vim.g.rnvimr_hide_gitignore = 1

			-- make ranger window take the full space of the editor
			vim.cmd([[
				let g:rnvimr_layout = {
					\ 'relative': 'editor',
					\ 'width': float2nr(1.0 * &columns),
					\ 'height': float2nr(1.0 * &lines) - 2,
					\ 'col': 0,
					\ 'row': 0,
					\ 'style': 'minimal'
					\ }
				let g:rnvimr_presets = [{}]
			]])
		end,
	},

	{
		"andymass/vim-matchup",
		event = "CursorMoved",
		config = function()
			vim.g.matchup_matchparen_offscreen = { method = "popup" }
		end,
	},

	{
		"chentoast/marks.nvim",
		config = function()
			require("marks").setup {
				-- which builtin marks to show. default {}
				-- builtin_marks = { ".", "<", ">", "^" },
				-- whether movements cycle back to the beginning/end of buffer. default true
				-- cyclic = true,
				default_mappings = false,
				mappings = {
					set = "ma",
					delete = "md",
					toggle = "mt",
					delete_line = "ml",
					delete_buf = "mb",
					prev = "mn",
					next = "me",
					preview = "mp",
					set_bookmark0 = "ma0",
					set_bookmark1 = "ma1",
					set_bookmark2 = "ma2",
					set_bookmark3 = "ma3",
					set_bookmark4 = "ma4",
					set_bookmark5 = "ma5",
					set_bookmark6 = "ma6",
					set_bookmark7 = "ma7",
					set_bookmark8 = "ma8",
					set_bookmark9 = "ma9",
				}

			}
		end,
	},
}

-- vim.keymap.set("n", "MD", ":lua require 'marks'.delete()<CR>") -- delete bookmarks

-- -- Autocommands (`:help autocmd`) <https://neovim.io/doc/user/autocmd.html>
-- vim.api.nvim_create_autocmd("FileType", {
--   pattern = "zsh",
--   callback = function()
--     -- let treesitter use bash highlight for zsh files as well
--     require("nvim-treesitter.highlight").attach(0, "bash")
--   end,
-- })

lvim.builtin.treesitter.matchup.enable = true

lvim.lsp.on_attach_callback = function(client, bufnr)
	require "lsp_signature".on_attach()
end

vim.keymap.set("n", "<C-o>", ":RnvimrToggle<CR>", { desc = "open ranger" })

vim.keymap.set("n", "GN", ":Gitsigns prev_hunk<CR>", { desc = "Git: goto previous hunk" })
vim.keymap.set("n", "GE", ":Gitsigns next_hunk<CR>", { desc = "Git: goto next hunk" })
vim.keymap.set("n", "GB", ":lua require 'gitsigns'.blame_line()<CR>", { desc = "Git: blame line" })
vim.keymap.set("n", "GP", ":lua require 'gitsigns'.preview_hunk()<CR>", { desc = "Git: preview hunk" })
vim.keymap.set("n", "GR", ":lua require 'gitsigns'.reset_hunk()<CR>", { desc = "Git: reset hunk" })
vim.keymap.set("n", "GF", ":lua require 'gitsigns'.reset_buffer()<CR>", { desc = "Git: reset buffer" })
vim.keymap.set("n", "GS", ":lua require 'gitsigns'.stage_hunk()<CR>", { desc = "Git: stage hunk" })
vim.keymap.set("n", "GU", ":lua require 'gitsigns'.undo_stage_hunk()<CR>", { desc = "Git: Undo hunk staging" })
vim.keymap.set("n", "GD", ":Gitsigns diffthis HEAD<CR>", { desc = "Git: diff the current change with the head" })

lvim.builtin.which_key.mappings.G = {
	name = "Git",
	G = { "<cmd>lua require 'lvim.core.terminal'.lazygit_toggle()<cr>", "Lazygit" },
	E = { "<cmd>lua require 'gitsigns'.next_hunk({navigation_message = false})<cr>", "Next Hunk" },
	N = { "<cmd>lua require 'gitsigns'.prev_hunk({navigation_message = false})<cr>", "Prev Hunk" },
	B = { "<cmd>lua require 'gitsigns'.blame_line()<cr>", "Blame" },
	P = { "<cmd>lua require 'gitsigns'.preview_hunk()<cr>", "Preview Hunk" },
	R = { "<cmd>lua require 'gitsigns'.reset_hunk()<cr>", "Reset Hunk" },
	r = { "<cmd>lua require 'gitsigns'.reset_buffer()<cr>", "Reset Buffer" },
	S = { "<cmd>lua require 'gitsigns'.stage_hunk()<cr>", "Stage Hunk" },
	U = {
		"<cmd>lua require 'gitsigns'.undo_stage_hunk()<cr>",
		"Undo Stage Hunk",
	},
	s = { "<cmd>Telescope git_status<cr>", "Open changed file" },
	b = { "<cmd>Telescope git_branches<cr>", "Checkout branch" },
	c = { "<cmd>Telescope git_commits<cr>", "Checkout commit" },
	C = {
		"<cmd>Telescope git_bcommits<cr>",
		"Checkout commit(for current file)",
	},
	D = {
		"<cmd>Gitsigns diffthis HEAD<cr>",
		"Git Diff",
	},
}
lvim.builtin.which_key.mappings.g = nil

-- LHS of toggle mappings in NORMAL mode
lvim.builtin.comment.toggler = {
	line = "CTL", -- Line-comment toggle keymap
	block = "CTB" -- Block-comment toggle keymap
}
-- LHS of operator-pending mappings in NORMAL and VISUAL mode
lvim.builtin.comment.opleader = {
	-- duplicate
	line = "CML", -- Line-comment keymap
	block = "CMB", -- Block-comment keymap
}
-- LHS of extra mappings
lvim.builtin.comment.extra = {
	above = 'CLN', -- Add comment on the line above
	below = 'CLE', -- Add comment on the line below
	eol = 'CA', -- Add comment at the end of line
}

-- Jumping to the prev/next todo/fixme comment
vim.keymap.set("n", "CE", function() require("todo-comments").jump_next({ keywords = { "TODO", "FIXME" } }) end,
	{ desc = "Next todo comment" })
vim.keymap.set("n", "CN", function() require("todo-comments").jump_prev({ keywords = { "TODO", "FIXME" } }) end,
	{ desc = "Previous todo comment" })
-- @TODO assign keys for :TodoQuickFix :TodoLocList :Trouble todo :TodoTelescope

vim.keymap.set("n", "DN", ":lua vim.diagnostic.goto_prev()<CR>", { desc = "Diagnostic: goto previous problem" })
vim.keymap.set("n", "DE", ":lua vim.diagnostic.goto_next()<CR>", { desc = "Diagnostic: goto next problem" })
vim.keymap.set("n", "DL",
	function()
		local float = vim.diagnostic.config().float

		if float then
			local config = type(float) == "table" and float or {}
			config.scope = "line"

			vim.diagnostic.open_float(config)
		end
	end
)
vim.keymap.set("n", "DT", ":Telescope diagnostics bufnr=0 theme=get_ivy<CR>", { desc = "Diagnostic: show problems" })
vim.keymap.set("n", "DH", ":lua vim.lsp.buf.hover()<CR>", { desc = "Diagnostic: show definition in a hovering window" })
vim.keymap.set("n", "DD", ":lua vim.lsp.buf.definition()<CR>", { desc = "Diagnostic: goto definition" })
vim.keymap.set("n", "DC", ":lua vim.lsp.buf.declaration()<CR>", { desc = "Diagnostic: goto declaration" })
vim.keymap.set("n", "DR", ":lua vim.lsp.buf.references()<CR>", { desc = "Diagnostic references" })
vim.keymap.set("n", "DI", ":lua vim.lsp.buf.implementation()<CR>", { desc = "Diagnostic implementation" })
vim.keymap.set("n", "DS", ":lua vim.lsp.buf.signature_help()<CR>", { desc = "Diagnostic signature help" })

-- lvim.builtin.which_key.mappings.l = lvim.builtin.which_key.mappings.L
lvim.builtin.which_key.mappings["D"] = lvim.builtin.which_key.mappings.l
lvim.builtin.which_key.mappings["D"] = {
	name = "LSP",
	a = { "<cmd>lua vim.lsp.buf.code_action()<cr>", "Code Action" },
	A = { "<cmd>lua vim.lsp.codelens.run()<cr>", "CodeLens Action" },
	d = { "<cmd>Telescope diagnostics<cr>", "Diagnostics" },
	T = { "<cmd>Telescope diagnostics bufnr=0 theme=get_ivy<cr>", "Buffer Diagnostics" },
	F = { "<cmd>lua require('lvim.lsp.utils').format()<cr>", "Format" },
	i = { "<cmd>LspInfo<cr>", "Info" },
	M = { "<cmd>Mason<cr>", "Mason Info" },
	N = {
		"<cmd>lua vim.diagnostic.goto_prev()<cr>",
		"Prev Diagnostic",
	},
	E = {
		"<cmd>lua vim.diagnostic.goto_next()<cr>",
		"Next Diagnostic",
	},
	L = {
		function()
			local float = vim.diagnostic.config().float

			if float then
				local config = type(float) == "table" and float or {}
				config.scope = "line"

				vim.diagnostic.open_float(config)
			end
		end,
		"Show line diagnostics"
	},
	Q = { "<cmd>lua vim.diagnostic.setloclist()<cr>", "Quickfix" },
	q = { "<cmd>Telescope quickfix<cr>", "Telescope Quickfix" },
	R = { "<cmd>lua vim.lsp.buf.rename()<cr>", "Rename" },
	S = { "<cmd>Telescope lsp_document_symbols<cr>", "Document Symbols" },
	s = {
		"<cmd>Telescope lsp_dynamic_workspace_symbols<cr>",
		"Workspace Symbols",
	}
}
-- lvim.builtin.which_key.vmappings.L = lvim.builtin.which_key.vmappings.l
-- lvim.builtin.which_key.vmappings.l = nil

-- lvim.builtin.which_key.mappings["l"] = {

-- }

-- Changing tabs
vim.keymap.set("n", "WN", ':BufferLineCyclePrev<CR>', { desc = "Goto previous tab" })
vim.keymap.set("n", "WE", ':BufferLineCycleNext<CR>', { desc = "Goto next tab" })
vim.keymap.set("n", "WI", ':BufferLineMovePrev<CR>', { desc = "Move the current tab to the left" })
vim.keymap.set("n", "WO", ':BufferLineMoveNext<CR>', { desc = "Move the current tab to the right" })
vim.keymap.set("n", "WQ", ':BufferKill<CR>', { desc = "Close the current tab" })
vim.keymap.set("n", "WA", ':tabnew<CR>', { desc = "Open new tab" })
vim.keymap.set("n", "WJ", ':BufferLinePick<CR>', { desc = "Jump to tab" })
vim.keymap.set("n", "WF", ':Telescope buffers<CR>', { desc = "Find a tab" })

lvim.builtin.which_key.mappings.W = {
	name = "Buffers",
	J = { "<cmd>BufferLinePick<cr>", "Jump" },
	F = { "<cmd>Telescope buffers previewer=false<cr>", "Find" },
	N = { "<cmd>BufferLineCyclePrev<cr>", "Previous" },
	E = { "<cmd>BufferLineCycleNext<cr>", "Next" },
	W = { "<cmd>noautocmd w<cr>", "Save without formatting (noautocmd)" },
	-- w = { "<cmd>BufferWipeout<cr>", "Wipeout" }, -- TODO: implement this for bufferline
	q = {
		"<cmd>bufferlinepickclose<cr>",
		"pick which buffer to close",
	},
	i = { "<cmd>bufferlinecloseleft<cr>", "close all to the left" },
	o = {
		"<cmd>bufferlinecloseright<cr>",
		"close all to the right",
	},
	d = {
		"<cmd>bufferlinesortbydirectory<cr>",
		"sort by directory",
	},
	x = {
		"<cmd>bufferlinesortbyextension<cr>",
		"sort by language",
	},
}
lvim.builtin.which_key.mappings.b = nil
-- clone current line(s)
lvim.builtin.which_key.mappings.c = { "yyp", "Duplicate current line & go to the next line" }
lvim.builtin.which_key.vmappings.c = { "yP", "Duplicate current selection" }
lvim.builtin.which_key.mappings.i = { "i <ESC>l", "Insert a space & get back to the normal mode" }
lvim.builtin.which_key.mappings.a = { "a <ESC>h", "Append a space & get back to the normal mode" }

lvim.builtin.which_key.mappings['<Tab>'] = { "<C-w>w", "Switch buffer" }

vim.keymap.set("x", "k", '<CMD>lua require("which-key").show("i", {mode = "x", auto = true})<CR>')

lvim.builtin.which_key.mappings["/"] = { "<CMD>lua require('Comment.api').toggle.linewise.current()<CR>j", "Comment" }
lvim.builtin.which_key.vmappings["/"] = {
	"<ESC><CMD>lua require('Comment.api').locked('comment.linewise')(vim.fn.visualmode())<CR>",
	"Comment" }

lvim.builtin.which_key.mappings["P"] = { "<CMD>Telescope projects<CR>", "Projects" }


lvim.builtin.which_key.mappings["E"] = {
	name = '+Editorstuff',
	r = { ":source ~/.config/lvim/config.lua<CR>", "Reload config.lua without quitting" },
	i = { ":set invlist<CR>", "Show empty characters" },
	h = { ":set hlsearch! hlsearch?<CR>", "Toggle search result highlighting" },
	t = { ":let _s=@/<Bar>:%s/\\s\\+$//e<Bar>:let @/=_s<Bar><CR>", "Remove triling spaces" }
}

o = { ":SymbolsOutline<CR>", "Toggle symbols outline panel" }

vim.keymap.set("n", "CD", "<Plug>(doge-comment-jump-forward)", { desc = "Add documentation comment" })
lvim.builtin.which_key.mappings['?'] = { ":DogeGenerate<CR>", "Add documentation comment" }

lvim.builtin.which_key.mappings["t"] = {
	name = "+Trouble",
	t = { "<CMD>TroubleToggle<CR>", "Trouble" },
	r = { "<CMD>TroubleToggle lsp_references<CR>", "References" },
	f = { "<CMD>Trouble lsp_definitions<CR>", "Definitions" },
	d = { "<CMD>TroubleToggle lsp_document_diagnostics<CR>", "Diagnostics" },
	q = { "<CMD>TroubleToggle quickfix<CR>", "QuickFix" },
	l = { "<CMD>TroubleToggle loclist<CR>", "LocationList" },
	w = { "<CMD>TroubleToggle lsp_workspace_diagnostics<CR>", "Workspace Diagnostics" },
}

-- vim.keymap.set("n", "SD", ":lua require('persistence').load()<CR>", { desc = "Load session for the current directory" })
-- vim.keymap.set("n", "SL", ":lua require('persistence').load({last = true})<CR>", { desc = "Load last session" })
-- vim.keymap.set("n", "SS", ":lua require('persistence').select()<CR>", { desc = "Select session to load" })
lvim.builtin.which_key.mappings["S"] = {
	name = "+Session",
	D = { "<CMD>lua require('persistence').load()<CR>", "Restore last session for current dir" },
	L = { "<CMD>lua require('persistence').load({ last = true })<CR>", "Restore last session" },
	S = { "<CMD>lua require('persistence').select()<CR>", "Select a session to load" },
	Q = { "<CMD>lua require('persistence').stop()<CR>", "Quit without saving session" },

	T = { '<cmd>lua require("spectre").toggle()<CR>', "Toggle Spectre" },
	W = { '<cmd>lua require("spectre").open_visual({select_word=true})<CR>', "Search current word" },
	F = { '<cmd>lua require("spectre").open_file_search({select_word=true})<CR>', "Search current word in the current file" },
}
lvim.builtin.which_key.vmappings.s = { '<esc><cmd>lua require("spectre").open_visual()<CR>', "Search selection " }

vim.cmd([[
	" {n}zee to fold the current and next n lines upward
	nnoremap zee :<C-U>exe "-" . v:count1 . ",.fo"<CR>
	" {n}znn to fold the current and next n lines downward
	nnoremap znn :<C-U>exe ".,+" . v:count1 . "fo"<CR>

	nnoremap <expr> DN &diff ? '[cz.' : 'dk'
	nnoremap <expr> DE &diff ? ']cz.' : 'dj'
	" after putting/getting the diff go the next diff and center the line vertically
	nnoremap <expr> DP &diff ? 'dp]cz.' : ''
	nnoremap <expr> DG &diff ? 'do]cz.' : ''
	nnoremap <expr> <C-q> &diff ? ':q<CR>:q<CR>' : ':call QuickFixToggle()<CR>'
	" Search for the selected text
	vnoremap // y/\V<C-R>=escape(@",'/\')<CR><CR>
	" Replace the selected text
	vnoremap \\ "hy:%s/\V<C-r>h//gc<left><left><left>
]])

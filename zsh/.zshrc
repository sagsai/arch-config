# Luke's config for the Zoomer Shell

# `man zshzle` gives the manpage containing the bindkey command
# `bindkey -l` followed by `bindkey -M <commandname>` e.g. `bindkey -M vicmd` gives the list of command under a menu

# Enable colors and change prompt:
autoload -U colors && colors	# Load colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "
setopt autocd		# Automatically cd into typed directory.
stty stop undef		# Disable ctrl-s to freeze terminal.
setopt interactive_comments

# History in cache directory:
HISTSIZE=10000000
SAVEHIST=10000000
HISTFILE="${XDG_CACHE_HOME:-$HOME/.cache}/zsh/history"

# Load aliases and shortcuts if existent.
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/shortcutrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/shortcutrc"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/zshnameddirrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/zshnameddirrc"

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.
eval "$(zoxide init zsh)"

# vi mode
bindkey -v
export KEYTIMEOUT=30

# Use vim keys in tab complete menu:
bindkey -M menuselect 'i' vi-backward-char
bindkey -M menuselect 'o' vi-forward-char
bindkey -M menuselect 'n' vi-up-line-or-history
bindkey -M menuselect 'e' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# Change cursor shape for different vi modes.
function zle-keymap-select () {
    case $KEYMAP in
        vicmd) echo -ne '\e[1 q';;      # block
        viins|main) echo -ne '\e[5 q';; # beam
    esac
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Use lf to switch directories and bind it to ctrl-o
lfcd () {
    tmp="$(mktemp -uq)"
    trap 'rm -f $tmp >/dev/null 2>&1' HUP INT QUIT TERM PWR EXIT
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
    fi
}
bindkey -s '^o' '^ulfcd\n'

bindkey -s '^a' '^ubc -lq\n'

bindkey -s '^f' '^ucd "$(dirname "$(fzf)")"\n'

bindkey '^[[3~' delete-char
bindkey '^R' history-incremental-search-backward
bindkey '^S' history-incremental-search-forward
bindkey -a '^R' history-incremental-search-backward
bindkey -a '^S' history-incremental-search-forward

bindkey -v '^?' backward-delete-char

# Get the key code for a key
# cat
# Show all current terminal key combinations using
# stty -a
# to change the keycode for a command
# stty werase ^H
# bindkey -l
# bindkey -M modename
# bindkey -LM modename
# man zshzle

# remove some keybindings from the vicmd keymap
# also can use the command `bindkey -M vicmd -r "string"` instead
bindkey -ar "j"
bindkey -ar "k"
bindkey -ar "l"
bindkey -ar "O"
bindkey -ar "C"

bindkey -a "n" up-line-or-history
bindkey -a "e" down-line-or-history
bindkey -a "i" vi-backward-char
bindkey -a "o" vi-forward-char
bindkey -a "N" beginning-of-buffer-or-history
bindkey -a "E" end-of-buffer-or-history
bindkey -a "a" vi-backward-word
bindkey -a "r" vi-backward-word-end
bindkey -a "s" vi-forward-word
bindkey -a "t" vi-forward-word-end
bindkey -a "A" vi-backward-blank-word
bindkey -a "R" vi-backward-blank-word-end
bindkey -a "S" vi-forward-blank-word
bindkey -a "T" vi-forward-blank-word-end
bindkey -a "x" vi-delete-char
bindkey -a "X" vi-backward-delete-char
bindkey -a "ki" vi-insert
bindkey -a "KI" vi-insert-bol
bindkey -a "ka" vi-add-next
bindkey -a "KA" vi-add-eol
bindkey -a "kr" vi-replace-chars
bindkey -a "KR" vi-replace
bindkey -a "ks" vi-substitute
bindkey -a "KS" vi-change-whole-line
bindkey -a "le" vi-open-line-below
bindkey -a "ln" vi-open-line-above
bindkey -a "u" undo
bindkey -a "U" redo
bindkey -a "h" vi-repeat-search
bindkey -a "H" vi-rev-repeat-search
bindkey -a "p" vi-put-after
bindkey -a "P" vi-put-before
bindkey -a "f" vi-find-next-char
bindkey -a "F" vi-find-prev-char
bindkey -a "b" vi-find-next-char-skip
bindkey -a "B" vi-find-prev-char-skip
# bindkey -a "kl" vi-down-case
# bindkey -a "KU" vi-up-case
# bindkey -a "KC" capitalize-word
# bindkey -a "ks" vi-oper-swap-case
# bindkey -a "kp" vi-swap-case

bindkey -a "da" vi-backward-kill-word
bindkey -a "dt" kill-word
bindkey -a "dw" delete-word
bindkey -a "dL" vi-kill-line
bindkey -a "dl" vi-kill-eol

bindkey -a "y" vi-yank
bindkey -a "yy" vi-yank-whole-line
bindkey -a "YE" vi-yank-eol
bindkey -a "YP" yank-pop

bindkey -a "cl" vi-change-eol
bindkey -a "Cw" down-case-word
bindkey -a "CW" up-case-word

autoload edit-command-line; zle -N edit-command-line
bindkey '^v' edit-command-line
bindkey -M vicmd '^[[3~' vi-delete-char
bindkey -M visual '^[[3~' vi-delete

# Load syntax highlighting; should be last.
source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh 2>/dev/null

# powerline-shell comand prompt
function powerline_precmd() {
    PS1="$(powerline-shell --shell zsh $?)"
}

function install_powerline_precmd() {
  for s in "${precmd_functions[@]}"; do
    if [ "$s" = "powerline_precmd" ]; then
      return
    fi
  done
  precmd_functions+=(powerline_precmd)
}

if [ "$TERM" != "linux" -a -x "$(command -v powerline-shell)" ]; then
    install_powerline_precmd
fi

# Add the ssh keys just after login
if [ ! -S ~/.ssh/ssh_auth_sock ]; then
  eval `ssh-agent`
  ln -sf "$SSH_AUTH_SOCK" ~/.ssh/ssh_auth_sock
fi
export SSH_AUTH_SOCK=~/.ssh/ssh_auth_sock
ssh-add -l > /dev/null || ssh-add

# export PATH="$HOME/.local/share/lunarvim/site/pack/lazy/opt/nvim-treesitter/plugin:$PATH"
